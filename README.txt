Jupyter python notebook from the article "Possibilistic Pathways and Decision-Making for Goal Achievement in Integrated Agritourism"

Alice DE LAPPARENT*, Rodolphe SABATIER, Sophie MARTIN, Cédric GAUCHEREL
*Corresponding author: alice.de-lapparent@inrae.fr; Ecodeveloppement, INRAE, France

Publication in Agronomy for Sustainable Development


Running the notebook with docker:
 - run in the command terminal: docker run -p 8000:8000 franckpommereau/ecco:0.4 jupyterhub
 - navigate in your web browser to: http://localhost:8000/
 - login as user "ecco" with password "ecco"
 - upload "Montrieux_NotebookVF-ecco.ipynb" and "MontrieuxEDEN.rr"
 - open "Montrieux_NotebookVF-ecco.ipynb" and run the notebook

